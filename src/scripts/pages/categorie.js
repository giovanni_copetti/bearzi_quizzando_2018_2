import AbsPage from "./AbsPage";

/**
 * le classi di pagina estendono AbsPage
 */
export default class Categorie extends AbsPage {
    constructor(pagedata) {
        console.log("Categorie");
        /** richiamiamo super per ultima per avere
         * tutte le variabili di istanza disponibili
         * nel metodo init
         */
        super(pagedata);
    }

    _init() {
        super._init();
    }
}