import Signal from "signals";

export default class Model {
    constructor() {
        /** questo controllo fa in modo che la classe
         * sia un Singleton, cioè che ha sempre solo un'istanza 
         * anche se viene fatto new in più parti del codice
         */
        if (!Model.instance) {
            this.onUtente = new Signal();
            this.onLogin = new Signal();
            Model.instance = this;
        }
        return Model.instance;
    }

    getUtente() {
        this.onUtente.dispatch({
            type: "onUtente", 
            data: this.utente
        });
    }
    
    login(username,password) {
        this.utente = {
            username: "utente1",
            avatar: "http://www.asdf.it/a.jpg",
            punteggio: 150,
            livello: 1,
            posizione: 5
        };
        this.onLogin.dispatch({
            type: "onLogin",
            data: this.utente
        });
        this.onUtente.dispatch({
            type: "onUtente", 
            data: this.utente
        });
    }

}
