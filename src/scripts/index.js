import '../styles/index.scss';
import Model from './Model';
import Home from './pages/Home';
import Categorie from './pages/Categorie';

class App {

  constructor() {
      console.log("new App");

      App.isCordova = (typeof cordova !== "undefined");
      if (App.isCordova) {
          // attendiamo il deviceready di cordova
          //document.addEventListener('deviceready', ()=>this._init(););
          document.addEventListener('deviceready',() => {
              console.log("deviceready");
              this._init();
          });
      } else {
        window.addEventListener('load',() => {
              console.log("ready");
              this._init();
          });
      }

      this.f7 = null;
      this.mainView = null;
      this.model = null;

      this.page_classes = {
        "Home": Home
      };
      this.pages_dict = {};

  }

  /**
   * funzione di inizializzazione generale
   */
  _init() {
    this.f7 = new Framework7({
      // App root element
      root: '#app',
      // App Name
      name: 'Quizzando 2018',
      // tema forzato su android
      theme: "md",
      // App id
      id: 'it.bearzi.quizzando',
      // Enable swipe panel
      panel: {
        swipe: 'right',
      },
      // Add default routes
      routes: [
        {
          path: '/categorie/',
          url: 'public/categorie.html',
          classname: Categorie
        },
        {
          path: '/classifica/',
          url: 'public/classifica.html',
        },
        {
          path: '/domanda/',
          url: 'public/domanda.html',
        },
      ],
      // ... other parameters
    });
    this.mainView = this.f7.views.create('.view-main', {
      routesBeforeEnter: function (to, from, resolve, reject) {
        console.log('routesBeforeEnter');
        resolve();
      },
      routesBeforeLeave: function (to, from, resolve, reject) {
        console.log('routesBeforeLeave');
        resolve();
      },
      on: {
        pageInit: (page) => {
          console.log('pageInit', page.name);
          try {
            this.pages_dict[page.route.path] = new page.route.route.classname(page);
          } catch (error) {
            console.error(error);
          }
        },
        pageBeforeIn: function (page) {
          console.log('pageBeforeIn', page.route.path);
        },
        pageBeforeRemove: (page) => {
          console.log('pageBeforeRemove',page.route.path);
          try {
            this.pages_dict[page.route.path].destroy();
            delete this.pages_dict[page.route.path];
          } catch (error) {
          }
        }
      }
    });
    this.model = new Model();
    this.model.login("fausto.sgobba@bearzi.it","");
    this.model.onError.addOnce(function () {console.log("error signal");});
  
    // TODO: togliere
    //mainView.router.navigate("/domanda/");
  }
}
App.isCordova = null;

window.myapp = new App();
window.$$ = Dom7;
